ENTITY testbench IS
END testbench;

ARCHITECTURE vectors OF testbench IS
COMPONENT inversor
port(a: IN BIT; f: OUT BIT);
END COMPONENT;

SIGNAL a, f: BIT;
FOR DUT: inversor USE ENTITY WORK.inversor(logica);

BEGIN
DUT: inversor PORT MAP (a, f);

stimulus: PROCESS
BEGIN
a <= '0';
wait for 100 ns;
a <= '1';
wait for 500 ns;
a <= '0';
wait for 1000 ns;
a <= '1';
wait for 100 ns;
END PROCESS;
END vectors; 
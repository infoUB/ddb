
--masael2000@gmail.com

ENTITY lab2 IS
	PORT(s_a, a_1, a_0: IN BIT; f_s, f_1, f_0: OUT BIT);
END lab2;

ARCHITECTURE logicaretard OF lab2 IS
BEGIN
	f_s <= s_a AFTER 5 ns;
	f_1 <= (s_a XOR a_1) OR (a_1 AND NOT a_0) AFTER 5 ns;
	f_0 <= a_0 AFTER 5 ns;
END logicaretard;

ARCHITECTURE estructural OF lab2 IS

COMPONENT id IS
	PORT(a: IN BIT;
		f: OUT BIT);
END COMPONENT;

COMPONENT inv IS
	PORT(a: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT and2 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT or2 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT xor2 IS
	PORT(a, b: IN BIT;
		z: OUT BIT);
END COMPONENT;

SIGNAL sumand1, sumand2, inva_0: BIT;
FOR DUT0: xor2 USE ENTITY WORK.xor2(logicaretard);
FOR DUT1: inv USE ENTITY WORK.inv(logicaretard);
FOR DUT2: and2 USE ENTITY WORK.and2(logicaretard);
FOR DUT3: or2 USE ENTITY WORK.or2(logicaretard);
FOR DUT4: id USE ENTITY WORK.id(logicaretard);
FOR DUT5: id USE ENTITY WORK.id(logicaretard);

BEGIN
DUT0: xor2 PORT MAP (s_a, a_1, sumand1);
DUT1: inv PORT MAP (a_0, inva_0);
DUT2: and2 PORT MAP (a_1, inva_0, sumand2);
DUT3: or2 PORT MAP (sumand1, sumand2, f_1);
DUT4: id PORT MAP (s_a, f_s);
DUT5: id PORT MAP (a_0, f_0);
END estructural;


ARCHITECTURE ifthen OF lab2 IS
BEGIN
	PROCESS(s_a, a_1, a_0)
	BEGIN
		IF s_a='0' AND a_1='0' AND a_0='0' THEN
			f_s <= '0' AFTER 5 ns;
			f_1 <= '0' AFTER 5 ns;
			f_0 <= '0' AFTER 5 ns;
		ELSIF s_a='0' AND a_1='0' AND a_0='1' THEN
			f_s <= '0' AFTER 5 ns;
			f_1 <= '0' AFTER 5 ns;
			f_0 <= '1' AFTER 5 ns;
		ELSIF s_a='0' AND a_1='1' AND a_0='0' THEN 
			f_s <= '0' AFTER 5 ns;
			f_1 <= '1' AFTER 5 ns;
			f_0 <= '0' AFTER 5 ns;
		ELSIF s_a='0' AND a_1='1' AND a_0='1' THEN 
			f_s <= '0' AFTER 5 ns;
			f_1 <= '1' AFTER 5 ns;
			f_0 <= '1' AFTER 5 ns;
		ELSIF s_a='1' AND a_1='0' AND a_0='0' THEN 
			f_s <= '1' AFTER 5 ns;
			f_1 <= '0' AFTER 5 ns;
			f_0 <= '0' AFTER 5 ns;
		ELSIF s_a='1' AND a_1='0' AND a_0='1' THEN 
			f_s <= '1' AFTER 5 ns;
			f_1 <= '1' AFTER 5 ns;
			f_0 <= '1' AFTER 5 ns;
		ELSIF s_a='1' AND a_1='1' AND a_0='0' THEN 		
			f_s <= '1' AFTER 5 ns;
			f_1 <= '1' AFTER 5 ns;
			f_0 <= '0' AFTER 5 ns;
		ELSIF s_a='1' AND a_1='1' AND a_0='1' THEN 
			f_s <= '1' AFTER 5 ns;
			f_1 <= '0' AFTER 5 ns;
			f_0 <= '1' AFTER 5 ns;
		END IF;
	END PROCESS;
END ifthen;



ENTITY banc_de_proves IS
END banc_de_proves;

ARCHITECTURE test OF banc_de_proves IS
COMPONENT lab2 IS
	PORT(s_a, a_1, a_0: IN BIT; f_s, f_1, f_0: OUT BIT);
END COMPONENT;

SIGNAL s_a, a_1, a_0, sort_fs_logica, sort_f1_logica, sort_f0_logica, sort_fs_estructural, sort_f1_estructural, sort_f0_estructural, sort_fs_ifthen, sort_f1_ifthen, sort_f0_ifthen: BIT;
FOR DUT0: lab2 USE ENTITY WORK.lab2(logicaretard);
FOR DUT1: lab2 USE ENTITY WORK.lab2(estructural);
FOR DUT2: lab2 USE ENTITY WORK.lab2(ifthen);

BEGIN
DUT0: lab2 PORT MAP (s_a, a_1, a_0, sort_fs_logica, sort_f1_logica, sort_f0_logica); 
DUT1: lab2 PORT MAP (s_a, a_1, a_0, sort_fs_estructural, sort_f1_estructural, sort_f0_estructural);
DUT2: lab2 PORT MAP (s_a, a_1, a_0, sort_fs_ifthen, sort_f1_ifthen, sort_f0_ifthen);


PROCESS(s_a, a_1, a_0)
BEGIN
a_0 <= NOT a_0 AFTER 50 ns;
a_1 <= NOT a_1 AFTER 100 ns;
s_a <= NOT s_a AFTER 200 ns;
END PROCESS;
END TEST;
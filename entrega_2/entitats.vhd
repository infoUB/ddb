-- RESPOSTES A LES Q�ESTIONS PLANTEJADES

-- 1.- Analitzant el codi veiem que el bit m�s significatiu (A) canvia cada 200 ms.
-- 	Aix� implica que, durant els 200 primers milisegons, beta ser� sempre 0, i que, com que
--	A segueix essent 0, inva ser� 1, i aix�, durant aquest temps podem dir que F = B amb un retard
--	de 6 ms. Ara b�, a l'instant 200 ms A passa a ser 1, i B i C a 0. A l'instant 203 ms alfa ja 
--	ser� 0, i beta tamb�, ja queinvc encara no haur� canviat de valor, de manera que a l'instant 
--	206 ms F valdr� 0. Per� just despr�s de l'instant 203 ms, invc passar� a valdre 1, de manera 
--	que a and invc evaluar� a 1 a l'instant 206 ms, de manera que a 209 ms alfa or beta = F tamb� 
--	evaluar� a 1. Aix� el bot �s produ�t pels 3 ms durant els quals invc encara no ha canviat 
--	(de 200 a 203), que arriben amb un retard de 6 ms degut a les portes and i or.
--
-- 2.D- Tal com passava en el cas d'exemple, a l'arquitectura estructural amb logicaretard es veuen 
--	diferents "rebots" de la senyal. En aquest cas no analitzarem tots els rebots per�, com ja hem
--	vist, s�n deguts a l'acumulaci� de retards de les funcions. Dit d'una altra manera, aix� passa
--	perqu� la senyal que hem passat d'entrada encara no ha arribat a la sortida, de manera que a les
--	portes intermitges encara estem treballant amb senyal residual o no actualitzada que fa variar la
--	sortida.
--
-- 3.E- Perqu� 5 ns �s molt m�s petit que el temps m�xim de retard que es pot acumular. Aix� 
--	fa que en algunes portes (com a la xor2 final, per exemple) arribi a una entrada un senyal que 
--	s'ha generat per a uns valors concrets de ent3, ent2, ent1 i ent0 i a la segona entrada de 
--	l'xor2 arribi una senyal generada per uns valors molt m�s recent, de manera que la taula de la
--	veritat no es complir�.






-- Entity funcio_2
ENTITY funcio_2 IS
	PORT(a, b, c, d: IN BIT; f: OUT BIT);
END funcio_2;

--Arquitectura logica sense retard, per fer servir com a base i debug
ARCHITECTURE logica_non_retard of funcio_2 IS
BEGIN
	f <= ((((NOT a) AND b AND (NOT c)) OR (b AND (NOT d)) OR (a AND c AND d) OR (a AND (NOT d))) AND (NOT (a OR (NOT d))))
	OR
	((NOT(((NOT a) AND b AND (NOT c)) OR (b AND (NOT d)) OR (a AND c AND d) OR (a AND (NOT d)))) AND (a OR (NOT d)));
END logica_non_retard;

--Arquitectura logica (Apartat 2.A)
ARCHITECTURE logica of funcio_2 IS
BEGIN
	f <= ((((NOT a) AND b AND (NOT c)) OR (b AND (NOT d)) OR (a AND c AND d) OR (a AND (NOT d))) AND (NOT (a OR (NOT d))))
	OR
	((NOT(((NOT a) AND b AND (NOT c)) OR (b AND (NOT d)) OR (a AND c AND d) OR (a AND (NOT d)))) AND (a OR (NOT d))) 
	AFTER 5 ns;
END logica;

--Arquitectura estructural (Apartat 2.C)
ARCHITECTURE estructural of funcio_2 IS

COMPONENT inv IS
	PORT(a: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT and2 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT and3 IS
	PORT(a,b,c: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT or2 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT or3 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT or4 IS
	PORT(a,b,c,d: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT xor2 IS
	PORT(a, b: IN BIT;
		z: OUT BIT);
END COMPONENT;

SIGNAL inva, invb, invc, invd, sumand1, sumand2, sumand3, sumand4, suma1, suma2: BIT;
FOR DUT1: inv USE ENTITY WORK.inv(logica);
FOR DUT2: inv USE ENTITY WORK.inv(logica);
FOR DUT3: inv USE ENTITY WORK.inv(logica);
FOR DUT4: inv USE ENTITY WORK.inv(logica);
FOR DUT5: and3 USE ENTITY WORK.and3(logica);
FOR DUT6: and2 USE ENTITY WORK.and2(logica);
FOR DUT7: and3 USE ENTITY WORK.and3(logica);
FOR DUT8: and2 USE ENTITY WORK.and2(logica);
FOR DUT9: or4 USE ENTITY WORK.or4(logica);
FOR DUT10: or2 USE ENTITY WORK.or2(logica);
FOR DUT11: xor2 USE ENTITY WORK.xor2(logica);


BEGIN
DUT1: inv PORT MAP (a, inva);
DUT2: inv PORT MAP (b, invb);
DUT3: inv PORT MAP (c, invc);
DUT4: inv PORT MAP (d, invd);
DUT5: and3 PORT MAP (inva, b, invc, sumand1);
DUT6: and2 PORT MAP (b, invd, sumand2);
DUT7: and3 PORT MAP (a, c, d, sumand3);
DUT8: and2 PORT MAP (a, invd, sumand4);
DUT9: or4 PORT MAP (sumand1, sumand2, sumand3, sumand4, suma1);
DUT10: or2 PORT MAP (a, invd, suma2);
DUT11: xor2 PORT MAP (suma1, suma2, f);
END estructural;

--Arquitectura estructural amb els components amb retard (Apartat 2.D)
ARCHITECTURE estructuralretard of funcio_2 IS

COMPONENT inv IS
	PORT(a: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT and2 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT and3 IS
	PORT(a,b,c: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT or2 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT or3 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT or4 IS
	PORT(a,b,c,d: IN BIT;
		z: OUT BIT);
END COMPONENT;

COMPONENT xor2 IS
	PORT(a,b: IN BIT;
		z: OUT BIT);
END COMPONENT;

SIGNAL inva, invb, invc, invd, sumand1, sumand2, sumand3, sumand4, suma1, suma2: BIT;
FOR DUT1: inv USE ENTITY WORK.inv(logicaretard);
FOR DUT2: inv USE ENTITY WORK.inv(logicaretard);
FOR DUT3: inv USE ENTITY WORK.inv(logicaretard);
FOR DUT4: inv USE ENTITY WORK.inv(logicaretard);
FOR DUT5: and3 USE ENTITY WORK.and3(logicaretard);
FOR DUT6: and2 USE ENTITY WORK.and2(logicaretard);
FOR DUT7: and3 USE ENTITY WORK.and3(logicaretard);
FOR DUT8: and2 USE ENTITY WORK.and2(logicaretard);
FOR DUT9: or4 USE ENTITY WORK.or4(logicaretard);
FOR DUT10: or2 USE ENTITY WORK.or2(logicaretard);
FOR DUT11: xor2 USE ENTITY WORK.xor2(logicaretard);



BEGIN
DUT1: inv PORT MAP (a, inva);
DUT2: inv PORT MAP (b, invb);
DUT3: inv PORT MAP (c, invc);
DUT4: inv PORT MAP (d, invd);
DUT5: and3 PORT MAP (inva, b, invc, sumand1);
DUT6: and2 PORT MAP (b, invd, sumand2);
DUT7: and3 PORT MAP (a, c, d, sumand3);
DUT8: and2 PORT MAP (a, invd, sumand4);
DUT9: or4 PORT MAP (sumand1, sumand2, sumand3, sumand4, suma1);
DUT10: or2 PORT MAP (a, invd, suma2);
DUT11: xor2 PORT MAP (suma1, suma2, f);

END estructuralretard;

-- Arquitectura estructural (Apartat 2.B)
ARCHITECTURE ifthen OF funcio_2 IS
BEGIN
	PROCESS(a, b, c, d)
	BEGIN
		IF a='0' AND b='0' AND c='0' AND d='0' THEN
			f <= '1' AFTER 5 ns;
		ELSIF a='0' AND b='0' AND c='0' AND d='1' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='0' AND b='0' AND c='1' AND d='0' THEN
			f <= '1' AFTER 5 ns;
		ELSIF a='0' AND b='0' AND c='1' AND d='1' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='0' AND b='1' AND c='0' AND d='0' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='0' AND b='1' AND c='0' AND d='1' THEN
			f <= '1' AFTER 5 ns;
		ELSIF a='0' AND b='1' AND c='1' AND d='0' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='0' AND b='1' AND c='1' AND d='1' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='1' AND b='0' AND c='0' AND d='0' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='1' AND b='0' AND c='0' AND d='1' THEN
			f <= '1' AFTER 5 ns;
		ELSIF a='1' AND b='0' AND c='1' AND d='0' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='1' AND b='0' AND c='1' AND d='1' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='1' AND b='1' AND c='0' AND d='0' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='1' AND b='1' AND c='0' AND d='1' THEN
			f <= '1' AFTER 5 ns;
		ELSIF a='1' AND b='1' AND c='1' AND d='0' THEN
			f <= '0' AFTER 5 ns;
		ELSIF a='1' AND b='1' AND c='1' AND d='1' THEN
			f <= '0' AFTER 5 ns;
		END IF;
	END PROCESS;
END ifthen;



-- Banc de proves
ENTITY banc_de_proves IS
END banc_de_proves;

ARCHITECTURE test OF banc_de_proves IS

COMPONENT funcio_2
port(a, b, c, d: IN BIT; f: OUT BIT);
END COMPONENT;




SIGNAL ent3, ent2, ent1, ent0, sort_logica, sort_logicaretard, sort_estructuralretard, sort_ifthen: BIT;

FOR DUT0: funcio_2 USE ENTITY WORK.funcio_2(logica);
FOR DUT1: funcio_2 USE ENTITY WORK.funcio_2(logicaretard);
FOR DUT2: funcio_2 USE ENTITY WORK.funcio_2(estructuralretard);
FOR DUT3: funcio_2 USE ENTITY WORK.funcio_2(ifthen);

BEGIN

DUT0: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_logica);
DUT1: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_logicaretard);
DUT2: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_estructuralretard);
DUT3: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_ifthen);

PROCESS(ent3, ent2, ent1, ent0)
BEGIN
ent0 <= NOT ent0 AFTER 50 ns;
ent1 <= NOT ent1 AFTER 100 ns;
ent2 <= NOT ent2 AFTER 200 ns;
ent3 <= NOT ent3 AFTER 400 ns;
END PROCESS;
END TEST;



-- INVERSOR  -----------------------------------------
ENTITY inv IS 
        PORT(a:IN BIT; z:OUT BIT);
END inv;


ARCHITECTURE logica OF inv IS
BEGIN
        z<= NOT a;
END logica;


ARCHITECTURE logica_retard OF inv IS
BEGIN
        z<= NOT a AFTER 3 ns;
END logica_retard;
------------------------------------------------------

-- AND2 -----------------------------------------
ENTITY and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END and2;


ARCHITECTURE logica OF and2 IS
BEGIN
        z<= a AND b;
END logica;

ARCHITECTURE logica_retard OF and2 IS
BEGIN
        z<= a AND b  AFTER 3 ns;
END logica_retard;
---------------------------------------------------

-- NOR2 -----------------------------------------
ENTITY nor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END nor2;


ARCHITECTURE logica OF nor2 IS
BEGIN
        z<= NOT (a OR b);
END logica;

ARCHITECTURE logica_retard OF nor2 IS
BEGIN
        z<= NOT (a OR b)  AFTER 5 ns;
END logica_retard;
---------------------------------------------------

-- LATCH D --------------------------------------
ENTITY Latch_D IS
PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END Latch_D;

ARCHITECTURE ifthen OF Latch_D IS
SIGNAL qint: BIT;
BEGIN

PROCESS (D,Clk,Pre,Clr)
BEGIN
	IF Clr='0' THEN 
		qint<='0' AFTER 3 ns;
     	ELSIF Pre='0' THEN 
		qint<='1' AFTER 3 ns;
     	ELSIF Clk='1' THEN
	     qint <= D AFTER 6 ns;

END IF;
END PROCESS;
Q<=qint; NO_Q<=NOT qint;
END ifthen;
--------------------------------------------------

-- FF_JK -----------------------------------------
ENTITY FF_JK IS
PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END FF_JK;

ARCHITECTURE ifthen OF FF_JK IS
SIGNAL qint: BIT;
BEGIN
PROCESS (J,K,Clk,Pre,Clr)
BEGIN
IF Clr='0' THEN qint<='0' AFTER 3 ns;
ELSE
	IF Pre='0' THEN qint<='1' AFTER 3 ns;
	ELSE
        	IF Clk'EVENT AND Clk='0' THEN
			IF J='0' AND K='0' THEN qint<=qint AFTER 6 ns;
        	        ELSIF J='0' AND K='1' THEN qint<='0' AFTER 6 ns;
        	        ELSIF J='1' AND K='0' THEN qint<='1' AFTER 6 ns;
        	        ELSIF J='1' AND K='1' THEN qint<= NOT qint AFTER 6 ns;
        	        END IF;
		
        	END IF;
        END IF;
END IF;

END PROCESS;
Q<=qint; NO_Q<=NOT qint;
END ifthen;
---------------------------------------------------

-- COMB ------------------------------------------
ENTITY comb IS
	PORT(x, Ck, preset, clear: IN BIT; z: OUT BIT);
END comb;

ARCHITECTURE estructural OF comb IS
COMPONENT inv IS
        PORT(a:IN BIT; z:OUT BIT);
END COMPONENT;

COMPONENT and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT nor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT Latch_D IS
	PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;

COMPONENT FF_JK IS
	PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;

FOR DUT1: inv USE ENTITY WORK.inv(logica_retard);
FOR DUT2: Latch_D USE ENTITY WORK.Latch_D(ifthen);
FOR DUT3: and2 USE ENTITY WORK.and2(logica_retard);
FOR DUT4: nor2 USE ENTITY WORK.nor2(logica_retard);
FOR DUT5: FF_JK USE ENTITY WORK.FF_JK(ifthen);

SIGNAL NO_x, Q1, NO_Q1, j, k, NO_z: BIT;

BEGIN
DUT1: inv PORT MAP(x, NO_x);
DUT2: Latch_D PORT MAP(x, Ck, preset, clear, Q1, NO_Q1);
DUT3: and2 PORT MAP(x, NO_Q1, j);
DUT4: nor2 PORT MAP(Q1, NO_x, k);
DUT5: FF_JK PORT MAP(j, k, Ck, preset, clear, z, NO_z);

END estructural;
------------------------------------------------------

-- TEST ---------------------------------------------
ENTITY testbench IS
END testbench;

ARCHITECTURE test OF testbench IS
	COMPONENT comb IS
		 PORT(x, Ck, preset, clear: IN BIT; z: OUT BIT);
	END COMPONENT;
FOR DUT1: comb USE ENTITY WORK.comb(estructural);
SIGNAL ent1, clock, pre, clr, sort: BIT;
BEGIN
DUT1: comb PORT MAP (ent1, clock, pre, clr, sort);
clr <= '0', '1' AFTER 10 ns;
pre <= '1';
clock <= NOT clock AFTER 50 ns;
ent1 <='0', '1' AFTER 75 ns, '0' AFTER 175 ns, '1' AFTER 255 ns, '0' AFTER 265 ns, '1' AFTER 275 ns, '0' AFTER 290 ns, '1' AFTER 325 ns, '0' AFTER 375 ns;
END test;
-------------------------------------------------------------

-- Aquest circuit, amb el cornograma donat, t� sortida 0 sempre. J i K sempre s�n 0 en els flancs de
--  baixada del clock, de manera que mai es variar� l'estat inicial, que en el nostre cas �s 0.
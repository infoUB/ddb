# Small program for evaluating logic expressions and create its truth table.

import operator
import string

class Infix:
    def __init__(self, function):
        self.function = function
    def __ror__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))
    def __or__(self, other):
        return self.function(other)
    def __rlshift__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))
    def __rshift__(self, other):
        return self.function(other)
    def __call__(self, value1, value2):
        return self.function(value1, value2)

then=Infix(lambda x,y: not x or y)
iff=Infix(lambda x,y: (not x or y) and (not y or x))
xor=Infix(lambda x,y: (x or y) and not(x and y))
print("LOGIC EXPRESSION EVALUATOR by Aniol Garcia \n")
print("HOW TO USE IT: ")
print("If you want to evaluate only one expression with given values, you only have to insert that specific expression.")
print("If you want to get the truth table, you should write the variables as 'v[0]', 'v[1]', ... , 'v[n]'.")
print("The predefined operators are: 'not', 'and', 'or', '|then|' and '|iff|'\n")

n_variables = eval(input('Number of variables to use (0 for an expression with given values): '))
x = input("Enter the expression: ")

if n_variables == 0:
    print(eval(x) == 1)
else:
    v = [0]*n_variables
    for i in range (0, (2**n_variables)):
        bits = [(i >> bit) & 1 for bit in range(n_variables - 1, -1, -1)]
        for j in range(0, len(bits)):
            v[j] = bits[j]

        print(bits, eval(x) == 1)


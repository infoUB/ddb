-- INVERSOR  -----------------------------------------
ENTITY inv IS 
        PORT(a:IN BIT; z:OUT BIT);
END inv;


ARCHITECTURE logica OF inv IS
BEGIN
        z<= NOT a;
END logica;


ARCHITECTURE logica_retard OF inv IS
BEGIN
        z<= NOT a AFTER 3 ns;
END logica_retard;
------------------------------------------------------

-- AND2 -----------------------------------------
ENTITY and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END and2;


ARCHITECTURE logica OF and2 IS
BEGIN
        z<= a AND b;
END logica;

ARCHITECTURE logica_retard OF and2 IS
BEGIN
        z<= a AND b  AFTER 3 ns;
END logica_retard;
---------------------------------------------------

-- and3 ------------------------------------------
ENTITY and3 IS
PORT(a, b, c: IN BIT; z: OUT BIT);
END and3;

ARCHITECTURE logica OF and3 IS
BEGIN
z <= (a AND b) AND c;
END logica;

ARCHITECTURE logica_retard OF and3 IS
BEGIN
z <= (a AND b) AND c AFTER 3 ns;
END logica_retard;
---------------------------------------------------

-- and4 --------------------------------------------
ENTITY and4 IS
PORT(a, b, c, d: IN BIT; z: OUT BIT);
END and4;

ARCHITECTURE logica OF and4 IS
BEGIN
z <= (a AND b) AND (c AND d);
END logica;

ARCHITECTURE logica_retard OF and4 IS
BEGIN
z <= (a AND b) AND (c AND d) AFTER 3 ns;
END logica_retard;
-----------------------------------------------


-- OR2 -----------------------------------------
ENTITY or2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END or2;


ARCHITECTURE logica OF or2 IS
BEGIN
        z<= (a OR b);
END logica;

ARCHITECTURE logica_retard OF or2 IS
BEGIN
        z<= (a OR b)  AFTER 3 ns;
END logica_retard;
--------------------------------------------------

-- or3 -------------------------------------------
ENTITY or3 IS
PORT(a, b, c: IN BIT; z: OUT BIT);
END or3;

ARCHITECTURE logica OF or3 IS
BEGIN
z <= (a OR b) OR c;
END logica;

ARCHITECTURE logica_retard OF or3 IS
BEGIN
z <= (a OR b) OR c AFTER 3 ns;
END logica_retard;
------------------------------------------------

-- XOR2 -----------------------------------------
ENTITY xor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END xor2;


ARCHITECTURE logica OF xor2 IS
BEGIN
        z<= (NOT a AND b) OR (a AND NOT b);
END logica;

ARCHITECTURE logica_retard OF xor2 IS
BEGIN
        z<= (NOT a AND b) OR (a AND NOT b) AFTER 3 ns;
END logica_retard;
---------------------------------------------------


-- FF T --------------------------------------------------------
ENTITY T_FF_Clr IS
PORT(T,Clk,Clr: IN BIT; Q: OUT BIT);
END T_FF_Clr;

ARCHITECTURE ifthen OF T_FF_Clr IS
SIGNAL qint: BIT;
BEGIN
PROCESS (T,Clk,Clr)
BEGIN
	IF Clr='0' THEN qint<='0' AFTER 3 ns;
	
	ELSIF Clk'EVENT THEN
		IF T='0' THEN qint<=qint AFTER 3 ns;
		ELSIF T='1' THEN qint<= NOT qint AFTER 3 ns;
		END IF;

	END IF;
END PROCESS;
Q<=qint;
END ifthen;
----------------------------------------------------

-- BLOC 1 ------------------------------------------
ENTITY Bloc1 IS
PORT(entX, ent2, ent1, ent0: IN BIT; sort2, sort1, sort0: OUT BIT);
END Bloc1;

ARCHITECTURE estructural OF Bloc1 IS

COMPONENT inv IS
        PORT(a:IN BIT; z:OUT BIT);
END COMPONENT;

COMPONENT and3 IS
        PORT(a,b, c:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT and4 IS
        PORT(a,b,c,d:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT or3 IS
        PORT(a,b,c:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT xor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

-- FOR DUT: USE ENTITY WORK.(logica_retard);
FOR DUT1: inv USE ENTITY WORK.inv(logica_retard);
FOR DUT2: inv USE ENTITY WORK.inv(logica_retard);
FOR DUT3: inv USE ENTITY WORK.inv(logica_retard);
FOR DUT4: inv USE ENTITY WORK.inv(logica_retard);
FOR DUT5: xor2 USE ENTITY WORK.xor2(logica_retard);
FOR DUT6: xor2 USE ENTITY WORK.xor2(logica_retard);
FOR DUT7: xor2 USE ENTITY WORK.xor2(logica_retard);
FOR DUT8: and3 USE ENTITY WORK.and3(logica_retard);
FOR DUT9: and3 USE ENTITY WORK.and3(logica_retard);
FOR DUT10: and4 USE ENTITY WORK.and4(logica_retard);
FOR DUT11: and3 USE ENTITY WORK.and3(logica_retard);
FOR DUT12: and3 USE ENTITY WORK.and3(logica_retard);
FOR DUT13: and4 USE ENTITY WORK.and4(logica_retard);
FOR DUT14: or3 USE ENTITY WORK.or3(logica_retard);
FOR DUT15: or3 USE ENTITY WORK.or3(logica_retard);

SIGNAL noX, no2, no1, no0, xor_s1, xor_s2, and_s1, and_s2, and_s3, and_s4, and_s5, and_s6, out0, out1, out2:BIT;

BEGIN
--DUT: PORT MAP();
DUT1: inv PORT MAP(entX, noX);
DUT2: inv PORT MAP(ent2, no2);
DUT3: inv PORT MAP(ent1, no1);
DUT4: inv PORT MAP(ent0, no0);

DUT5: xor2 PORT MAP(entX, ent2, xor_s1);
DUT6: xor2 PORT MAP(xor_s1, no1, xor_s2);
DUT7: xor2 PORT MAP(xor_s2, ent0, out0);

DUT8: and3 PORT MAP(ent1, no0, entX, and_s1);
DUT9: and3 PORT MAP(ent2, ent1, ent0, and_s2);
DUT10: and4 PORT MAP(no2, no1, ent0, noX, and_s3);


DUT11: and3 PORT MAP(ent2, no1, no0, and_s4);
DUT12: and3 PORT MAP(no1, no0, entX, and_s5);
DUT13: and4 PORT MAP(no2, ent1, no0, noX, and_s6);


DUT14: or3 PORT MAP(and_s1, and_s2, and_s3, out1);
DUT15: or3 PORT MAP(and_s4, and_s5, and_s6, out2);

sort0 <= out0;
sort1 <= out1;
sort2 <= out2;

END estructural;


------------------------------------------------------

-- MAQUINA -------------------------------------------
ENTITY MAQUINA IS
	PORT(X, clock: IN BIT; z2, z1, z0: OUT BIT);
END MAQUINA;

ARCHITECTURE estructural of MAQUINA IS
SIGNAL q0, q1, q2: BIT;
COMPONENT Bloc1 IS
	PORT(entX, ent2, ent1, ent0: IN BIT; sort2, sort1, sort0: OUT BIT);
END COMPONENT;

COMPONENT T_FF_Clr IS
	PORT(T,Clk,Clr: IN BIT; Q: OUT BIT);
END COMPONENT;

COMPONENT inv IS
        PORT(a:IN BIT; z:OUT BIT);
END COMPONENT;

COMPONENT and3 IS
        PORT(a,b, c:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT or2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;


FOR DUT1: Bloc1 USE ENTITY WORK.Bloc1(estructural);
FOR DUT2: or2 USE ENTITY WORK.or2(logica_retard);
FOR DUT3: inv USE ENTITY WORK.inv(logica_retard);
FOR DUT4: and3 USE ENTITY WORK.and3(logica_retard);
FOR DUT5: or2 USE ENTITY WORK.or2(logica_retard);
FOR DUT6: T_FF_Clr USE ENTITY WORK.T_FF_Clr(ifthen);
FOR DUT7: T_FF_Clr USE ENTITY WORK.T_FF_Clr(ifthen);
FOR DUT8: T_FF_Clr USE ENTITY WORK.T_FF_Clr(ifthen);


SIGNAL ent2, ent1, no_q0, ent0, sort2, sort1, sort0, or_pre1, and_pre2, or_pre2, out0, out1, out2: BIT;
BEGIN
DUT1: Bloc1 PORT MAP(X, q2, q1, q0, sort2, sort1, sort0);
DUT2: or2 PORT MAP(X, sort0, or_pre1);
DUT3: inv PORT MAP(q0, no_q0);
DUT4: and3 PORT MAP(no_q0, X, q2, and_pre2);
DUT5: or2 PORT MAP(and_pre2, sort1, or_pre2);
DUT6: T_FF_Clr PORT MAP(or_pre1, clock, '1', q0);
DUT7: T_FF_Clr PORT MAP(or_pre2, clock, '1', q1);
DUT8: T_FF_Clr PORT MAP(sort2, clock, '1', q2);

z2 <= q2;
z1 <= q1;
z0 <= q0;

END ESTRUCTURAL; 
--------------------------------------------------------

-- TESTBENCH --------------------------------------------
ENTITY bdp IS
END bdp;

ARCHITECTURE test OF bdp IS
COMPONENT MAQUINA IS
	PORT(X, clock: IN BIT; z2, z1, z0: OUT BIT);
END COMPONENT;


SIGNAL X, clock, z2, z1, z0: BIT;

FOR DUT1: MAQUINA USE ENTITY WORK.MAQUINA(estructural);
BEGIN
DUT1: MAQUINA PORT MAP(X, clock, z2, z1, z0);
clock <= NOT clock AFTER 50 ns;
X <= NOT X AFTER 650 ns;
END test;

-- En aquest cas, els estats corresponen amb les sortides.
---------------------------------------------------------------
-- Els priumers 400 ns la sortida s�n els 8 primers nombres de gray. Deixem passar 250 ns tamb� amb
-- 	X = 0, de manera que ens movem a l'estat F (amb sortida 111) on podem canviar X = 1, de manera
-- 	que es comportada com a contador descendent (s'anir� restant 1 a la sortida) fins que als 1050 ns
-- 	tornem a l'estat inicial (hem arribat a 0)
-- Amb entrada X = 0, maquina �s un contador de Gray de 3 bits, i amb X = 1 �s contador binari des de 7 fins a 0 (contador 
-- 	decreixent modul 8).

-- �s m�quina de moore, ja que la sortida v� donada per l'estat.

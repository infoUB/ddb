-- INVERSOR  -----------------------------------------
ENTITY inv IS 
        PORT(a:IN BIT; z:OUT BIT);
END inv;


ARCHITECTURE logica OF inv IS
BEGIN
        z<= NOT a;
END logica;


ARCHITECTURE logica_retard OF inv IS
BEGIN
        z<= NOT a AFTER 4 ns;
END logica_retard;
------------------------------------------------------

-- AND2 -----------------------------------------
ENTITY and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END and2;


ARCHITECTURE logica OF and2 IS
BEGIN
        z<= a AND b;
END logica;

ARCHITECTURE logica_retard OF and2 IS
BEGIN
        z<= a AND b  AFTER 4 ns;
END logica_retard;
---------------------------------------------------

-- XOR2 -----------------------------------------
ENTITY xor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END xor2;


ARCHITECTURE logica OF xor2 IS
BEGIN
        z<= (NOT a AND b) OR (a AND NOT b);
END logica;

ARCHITECTURE logica_retard OF xor2 IS
BEGIN
        z<= (NOT a AND b) OR (a AND NOT b) AFTER 4 ns;
END logica_retard;
---------------------------------------------------

-- FF_JK -----------------------------------------
ENTITY FF_JK IS
PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END FF_JK;

ARCHITECTURE ifthen OF FF_JK IS
SIGNAL qint: BIT;
BEGIN
PROCESS (J,K,Clk,Pre,Clr)
BEGIN
IF Clr='0' THEN qint<='0' AFTER 4 ns;
ELSE
	IF Pre='0' THEN qint<='1' AFTER 4 ns;
	ELSE
        	IF Clk'EVENT AND Clk='1' THEN
			IF J='0' AND K='0' THEN qint<=qint AFTER 4 ns;
        	        ELSIF J='0' AND K='1' THEN qint<='0' AFTER 4 ns;
        	        ELSIF J='1' AND K='0' THEN qint<='1' AFTER 4 ns;
        	        ELSIF J='1' AND K='1' THEN qint<= NOT qint AFTER 4 ns;
        	        END IF;
		
        	END IF;
        END IF;
END IF;

END PROCESS;
Q<=qint; NO_Q<=NOT qint;
END ifthen;

-- circuit --------------------------------------------
ENTITY circuit IS
PORT(X, clock: IN BIT; Z2, Z1, Z0: OUT BIT);
END circuit;

ARCHITECTURE estructural of circuit IS

COMPONENT inv IS
        PORT(a:IN BIT; z:OUT BIT);
END COMPONENT;

COMPONENT and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT xor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT FF_JK IS
	PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;

FOR DUT1: xor2 USE ENTITY WORK.xor2(logica_retard);
FOR DUT2: inv USE ENTITY WORK.inv(logica_retard);
FOR DUT3: and2 USE ENTITY WORK.and2(logica_retard);
FOR DUT4: FF_JK USE ENTITY WORK.FF_JK(ifthen);
FOR DUT5: FF_JK USE ENTITY WORK.FF_JK(ifthen);
FOR DUT6: FF_JK USE ENTITY WORK.FF_JK(ifthen);

SIGNAL no_xor, J1, J2, no_Z2, no_Z1, no_Z0, int_z2, int_z1, int_z0, int_J1, int_J2: BIT;

BEGIN
DUT1: xor2 PORT MAP(X, int_z0, no_xor);
DUT2: inv PORT MAP(no_xor, int_J1);
DUT3: and2 PORT MAP(int_J1, int_z1, int_J2);
DUT4: FF_JK PORT MAP(int_J2, int_J2, clock, '1', '1', int_z2, no_Z2); 
DUT5: FF_JK PORT MAP(int_J1, int_J1, clock, '1', '1', int_z1, no_Z1); 
DUT6: FF_JK PORT MAP(X, '1' , clock, '1', '1', int_z0, no_Z0); 

Z2 <= int_z2;
Z1 <= int_z1;
Z0 <= int_z0;

END estructural;

-- TEST ---------------------------------------------
ENTITY testbench IS
END testbench;

ARCHITECTURE test OF testbench IS
	COMPONENT circuit IS
		 PORT(x, clock: IN BIT; Z2, Z1, Z0: OUT BIT);
	END COMPONENT;
FOR DUT1: circuit USE ENTITY WORK.circuit(estructural);
SIGNAL ent1, clock, sort2, sort1, sort0: BIT;
BEGIN
DUT1: circuit PORT MAP (ent1, clock, sort2, sort1, sort0);
clock <= NOT clock AFTER 50 ns;
ent1 <= NOT ent1 AFTER 400 ns;

END test;
-------------------------------------------------------------

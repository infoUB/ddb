-- DEC1A2  -----------------------------------------
ENTITY dec2a4 IS 
        PORT(control, dada1, dada0:IN BIT; sortida3, sortida2, sortida1, sortida0:OUT BIT);
END dec2a4;

ARCHITECTURE estructural OF dec2a4 IS
	COMPONENT dec1a2 is
		PORT(d, e:IN BIT; z1, z0:OUT BIT);
	END COMPONENT;
	COMPONENT inv is
		PORT(a:IN BIT; z:OUT BIT);
	END COMPONENT;

	COMPONENT and2 is
		PORT(a,b:IN BIT; z:OUT BIT);
	END COMPONENT;

FOR DUT1: inv USE ENTITY WORK.inv(logica);
FOR DUT2: and2 USE ENTITY WORK.and2(logica);
FOR DUT3: and2 USE ENTITY WORK.and2(logica);
FOR DUT4: dec1a2 USE ENTITY WORK.dec1a2(ifthen);
FOR DUT5: dec1a2 USE ENTITY WORK.dec1a2(estructural);

SIGNAL dada1_inv, prod1, prod2:BIT;

BEGIN
DUT1: inv PORT MAP(dada1, dada1_inv);
DUT2: and2 PORT MAP(dada1_inv, control, prod1);
DUT3: and2 PORT MAP(dada1, control, prod2);
DUT4: dec1a2 PORT MAP(dada0, prod1, sortida1, sortida0);
DUT5: dec1a2 PORT MAP(dada0, prod2, sortida3, sortida2);
END estructural;

-- BANC DE PROVES  -----------------------------------------
ENTITY banc_de_proves IS
END banc_de_proves;

ARCHITECTURE testeig OF banc_de_proves IS
	COMPONENT dec2a4 IS
		PORT(control, dada1, dada0:IN BIT; sortida3, sortida2, sortida1, sortida0:OUT BIT);
	END COMPONENT;

FOR DUT1: dec2a4 USE ENTITY WORK.dec2a4(estructural);
SIGNAL control, dada1, dada0, sortida3, sortida2, sortida1, sortida0: BIT;
BEGIN
DUT1: dec2a4 PORT MAP(control, dada1, dada0, sortida3, sortida2, sortida1, sortida0);

PROCESS(control, dada1, dada0)
BEGIN
	dada0 <= NOT dada0 after 50 ns;
	dada1 <= NOT dada1 after 100 ns;
	control <= NOT control after 200 ns;
END PROCESS;
END testeig;
-- INVERSOR  -----------------------------------------
ENTITY inv IS 
        PORT(a:IN BIT; z:OUT BIT);
END inv;


ARCHITECTURE logica OF inv IS
BEGIN
        z<= NOT a;
END logica;


ARCHITECTURE logica_retard OF inv IS
BEGIN
        z<= NOT a AFTER 5 ns;
END logica_retard;


-- AND2 -----------------------------------------
ENTITY and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END and2;


ARCHITECTURE logica OF and2 IS
BEGIN
        z<= a AND b;
END logica;

ARCHITECTURE logica_retard OF and2 IS
BEGIN
        z<= a AND b  AFTER 5 ns;
END logica_retard;

-- DEC1A2  -----------------------------------------
ENTITY dec1a2 IS 
        PORT(d, e:IN BIT; z1, z0:OUT BIT);
END dec1a2;


ARCHITECTURE ifthen OF dec1a2 IS
BEGIN
	PROCESS(e, d)
	BEGIN
		IF e='0' AND d='0' THEN
			z1 <= '0' AFTER 3 ns;
			z0 <= '0' AFTER 3 ns;
		ELSIF e='0' AND d='1' THEN
			z1 <= '0' AFTER 3 ns;
			z0 <= '0' AFTER 3 ns;
		ELSIF e='1' AND d='0' THEN
			z1 <= '0' AFTER 3 ns;
			z0 <= '1' AFTER 3 ns;
		ELSIF e='1' AND d='1' THEN
			z1 <= '1' AFTER 3 ns;
			z0 <= '0' AFTER 3 ns;
		END IF;
	END PROCESS;
END ifthen;

ARCHITECTURE estructural OF dec1a2 IS
	COMPONENT inv is
		PORT(a:IN BIT; z:OUT BIT);
	END COMPONENT;

	COMPONENT and2 is
		PORT(a,b:IN BIT; z:OUT BIT);
	END COMPONENT;

FOR DUT1: inv USE ENTITY WORK.inv(logica);
FOR DUT2: and2 USE ENTITY WORK.and2(logica);
FOR DUT3: and2 USE ENTITY WORK.and2(logica);

SIGNAL d_inv:BIT;

BEGIN
DUT1: inv PORT MAP(d, d_inv);
DUT2: and2 PORT MAP(e, d_inv, z0);
DUT3: and2 PORT MAP(e, d, z1);
END estructural;


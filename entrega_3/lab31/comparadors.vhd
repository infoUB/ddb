
-- INVERSOR  -----------------------------------------
ENTITY inv IS 
        PORT(a:IN BIT; z:OUT BIT);
END inv;


ARCHITECTURE logica OF inv IS
BEGIN
        z<= NOT a;
END logica;


ARCHITECTURE logica_retard OF inv IS
BEGIN
        z<= NOT a AFTER 5 ns;
END logica_retard;


-- AND2 -----------------------------------------
ENTITY and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END and2;


ARCHITECTURE logica OF and2 IS
BEGIN
        z<= a AND b;
END logica;

ARCHITECTURE logica_retard OF and2 IS
BEGIN
        z<= a AND b  AFTER 5 ns;
END logica_retard;

-- OR2 -----------------------------------------
ENTITY or2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END or2;


ARCHITECTURE logica OF or2 IS
BEGIN
        z<= a OR b;
END logica;

ARCHITECTURE logica_retard OF or2 IS
BEGIN
        z<= a OR b  AFTER 5 ns;
END logica_retard;


-- XOR2 -----------------------------------------
ENTITY xor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END xor2;


ARCHITECTURE logica OF xor2 IS
BEGIN
        z<= (NOT a AND b) OR (a AND NOT b);
END logica;

ARCHITECTURE logica_retard OF xor2 IS
BEGIN
        z<= (NOT a AND b) OR (a AND NOT b) AFTER 5 ns;
END logica_retard;

-- NOR2 -----------------------------------------
ENTITY nor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END nor2;


ARCHITECTURE logica OF nor2 IS
BEGIN
        z<= NOT (a OR b);
END logica;

ARCHITECTURE logica_retard OF nor2 IS
BEGIN
        z<= NOT (a OR b) AFTER 5 ns;
END logica_retard;

-- IDENTITAT ------------------------------------
ENTITY id IS
        PORT(a: IN BIT; f: OUT BIT);
END id;

ARCHITECTURE logica OF id IS
BEGIN
        f <= a;
END logica;

ARCHITECTURE logica_retard OF id IS
BEGIN
        f <= a AFTER 5 ns;
END logica_retard;


-- COMPARADOR 2 BITS ----------------------------
ENTITY comp2 IS 
        PORT(a, b:IN BIT; e,g,l:OUT BIT);
END comp2;


ARCHITECTURE ifthen OF comp2 IS
BEGIN
	PROCESS(a, b)
	BEGIN
		IF a='0' AND b='0' THEN
			e <= '1' AFTER 3 ns;
			g <= '0' AFTER 3 ns;
			l <= '0' AFTER 3 ns;
		ELSIF a='0' AND b='1' THEN
			e <= '0' AFTER 3 ns;
			g <= '0' AFTER 3 ns;
			l <= '1' AFTER 3 ns;
		ELSIF a='1' AND b='0' THEN
			e <= '0' AFTER 3 ns;
			g <= '1' AFTER 3 ns;
			l <= '0' AFTER 3 ns;
		ELSIF a='1' AND b='1' THEN
			e <= '1' AFTER 3 ns;
			g <= '0' AFTER 3 ns;
			l <= '0' AFTER 3 ns;
		END IF;
	END PROCESS;
END ifthen;

ARCHITECTURE estructural OF comp2 IS
	COMPONENT inv is
		PORT(a:IN BIT; z:OUT BIT);
	END COMPONENT;

	COMPONENT and2 is
		PORT(a,b:IN BIT; z:OUT BIT);
	END COMPONENT;

	COMPONENT nor2 is
		PORT(a,b:IN BIT; z:OUT BIT);
	END COMPONENT;
	
	COMPONENT id is
		PORT(a:IN BIT; f:OUT BIT);
	END COMPONENT;

FOR DUT1: inv USE ENTITY WORK.inv(logica);
FOR DUT2: inv USE ENTITY WORK.inv(logica);
FOR DUT3: and2 USE ENTITY WORK.and2(logica);
FOR DUT4: and2 USE ENTITY WORK.and2(logica);
FOR DUT5: nor2 USE ENTITY WORK.nor2(logica);
FOR DUT6: id USE ENTITY WORK.id(logica);
FOR DUT7: id USE ENTITY WORK.id(logica);

SIGNAL a_inv, b_inv, g_tmp, l_tmp:BIT;

BEGIN
DUT1: inv PORT MAP(a, a_inv);
DUT2: inv PORT MAP(b, b_inv);
DUT3: and2 PORT MAP(a, b_inv, g_tmp);
DUT4: and2 PORT MAP(a_inv, b, l_tmp);
DUT5: nor2 PORT MAP(g_tmp, l_tmp, e);
DUT6: id PORT MAP(g_tmp, g);
DUT7: id PORT MAP(l_tmp, l);
END estructural;


-- COMPARADOR 4 BITS ----------------------------
ENTITY comp4 IS 
        PORT(a1, a0, b1, b0:IN BIT; e,g,l:OUT BIT);
END comp4;
ARCHITECTURE estructural OF comp4 IS
	COMPONENT comp2 is
		PORT(a, b:IN BIT; e,g,l:OUT BIT);
	END COMPONENT;

	COMPONENT and2 is
		PORT(a,b:IN BIT; z:OUT BIT);
	END COMPONENT;

	COMPONENT or2 is
		PORT(a,b:IN BIT; z:OUT BIT);
	END COMPONENT;

FOR DUT1: comp2 USE ENTITY WORK.comp2(estructural);
FOR DUT2: comp2 USE ENTITY WORK.comp2(estructural);
FOR DUT3: and2 USE ENTITY WORK.and2(logica);
FOR DUT4: or2 USE ENTITY WORK.or2(logica);
FOR DUT5: and2 USE ENTITY WORK.and2(logica);
FOR DUT6: and2 USE ENTITY WORK.and2(logica);
FOR DUT7: or2 USE ENTITY WORK.or2(logica);

SIGNAL g0, l0, e0, g1, l1, e1, prod1, prod2:BIT;

BEGIN
DUT1: comp2 PORT MAP(a0, b0, e0, g0, l0);
DUT2: comp2 PORT MAP(a1, b1, e1, g1, l1);
DUT3: and2 PORT MAP(g0, e1, prod1);
DUT4: or2 PORT MAP(prod1, g1, g);
DUT5: and2 PORT MAP(e0, e1, e);
DUT6: and2 PORT MAP(e1, l0, prod2);
DUT7: or2 PORT MAP(prod2, l1, l);
END estructural;

ENTITY banc_proves IS                           
END banc_proves;

ARCHITECTURE test OF banc_proves IS
	COMPONENT comp4 IS
		PORT(a1, a0, b1, b0:IN BIT; e,g,l:OUT BIT);
	END COMPONENT;
FOR DUT1: comp4 USE ENTITY WORK.comp4(estructural);


SIGNAL a1, a0, b1, b0, e, g, l: BIT;
BEGIN
DUT1: comp4 PORT MAP(a1, a0, b1, b0,e,g,l);


PROCESS(a0, a1, b0, b1)
BEGIN
	a0 <= NOT a0 after 50 ns;
	a1 <= NOT a1 after 100 ns;
	b0 <= NOT b0 after 200 ns;
	b1 <= NOT b1 after 400 ns;
END PROCESS;
END test;